from gpa import GPA,Course

import pytest



def test_calculate_gpa():
    gpa = GPA()
    gpa.addCourse(Course("Math", "A", 3))
    gpa.addCourse(Course("English", "B", 4))
    gpa.addCourse(Course("History", "C", 3))
    assert gpa.calculate_gpa() == pytest.approx(3.0, 0.01)

def test_calculate_gpa_with_no_grades():
    gpa = GPA()
    assert gpa.calculate_gpa() == 0

def test_add_grade():
    gpa = GPA()
    gpa.addCourse(Course("Math", "A", 3))
    gpa.addCourse(Course("English", "B", 4))
    gpa.addCourse(Course("History", "C", 3))
    gpa.addCourse(Course("Physics", "A", 4))
    assert gpa.calculate_gpa() == pytest.approx(3.28, 0.01)


