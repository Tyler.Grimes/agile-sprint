import pytest
from gpa import final_grade_calculator

def test_final_calculator_boundry1():
    # Test case 1: Final weight is 0
    assert final_grade_calculator(0.8, 0.9, 0) == 0.0
    
def test_final_calculator_boundry2():    
    # Test case 2: Current grade, desired grade, and final weight are all 0
    assert final_grade_calculator(0, 0, 0) == 0.0

def test_final_calculator_all1(): 
    # Test case 3: Current grade, desired grade, and final weight are all 1
    assert final_grade_calculator(1, 1, 1) == 'You will need to get a 100.0% on your final to achieve a 100% in the class.'

def test_final_calculator_boundry3(): 
    # Test case 4: Current grade is 0, desired grade is 1, and final weight is 0.5
    assert final_grade_calculator(0, 1, 0.5) == 'You will need to get a 200.0% on your final to achieve a 100% in the class.'

def test_final_calculator_normal1(): 
    # Test case 5: Current grade is 0.5, desired grade is 0.75, and final weight is 0.25
    assert final_grade_calculator(0.5, 0.75, 0.25) == 'You will need to get a 150.0% on your final to achieve a 75.0% in the class.'

def test_final_calculator_normal2(): 
    # Test case 6: Current grade is 0.75, desired grade is 0.8, and final weight is 0.1
    assert final_grade_calculator(0.75, 0.8, 0.1) == 'You will need to get a 125.0% on your final to achieve a 80.0% in the class.'




