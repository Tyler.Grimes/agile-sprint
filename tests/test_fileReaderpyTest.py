import pytest

def test_read_courses():
    file_path = "path/to/file.txt"  

    courses = []
    with open(file_path, "r") as file:
        for line in file:
            courses.append(line.strip())

    # Assert the expected courses
    assert len(courses) == 3
    assert courses[0] == "Math"
    assert courses[1] == "Science"
    assert courses[2] == "History"
