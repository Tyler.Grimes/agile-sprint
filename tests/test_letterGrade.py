from gpa import GPA
import pytest
test = GPA()

def test_D():
    assert test.GPAtoLetter(1.00) == "D-"

def test_D():
    assert test.GPAtoLetter(1.02) == "D"

def test_D():
    assert test.GPAtoLetter(1.35) == "D+"

def test_C():
    assert test.GPAtoLetter(1.7) == "C-"

def test_C():
    assert test.GPAtoLetter(2.00) == "C"

def test_C():
    assert test.GPAtoLetter(2.35) == "C+"

def test_B():
    assert test.GPAtoLetter(2.70) == "B-"

def test_B():
    assert test.GPAtoLetter(3.00) == "B"

def test_B():
    assert test.GPAtoLetter(3.35) == "B+"

def test_A():
    assert test.GPAtoLetter(3.75) == "A"


