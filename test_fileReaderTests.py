import unittest

class TestFileRead(unittest.TestCase):
    def test_file_read(self):
        file_path = "path/to/your/file.txt"  # Replace with the actual path to your file

        try:
            with open(file_path, "r") as file:
                courses = [line.strip() for line in file]

            # Assert that the courses list is not empty
            self.assertNotEqual(len(courses), 0)

            # Assert the expected values for specific lines in the file
            self.assertEqual(courses[0], "Course 1")
            self.assertEqual(courses[1], "Course 2")
            self.assertEqual(courses[2], "Course 3")
            # Add more assertions for other lines in the file

        except FileNotFoundError:
            self.fail(f"File not found: {file_path}")
        except IOError as e:
            self.fail(f"Error reading the file: {e}")

if __name__ == '__main__':
    unittest.main()
