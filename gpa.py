class Course:
    def __init__(self, name, grade, credit_hours):
        self.name = name
        self.grade = grade
        self.credit_hours = credit_hours

    def getName(self):
        return self.name

    def getGrade(self):
        return self.grade

    def getCreditHours(self):
        return self.credit_hours

# Class that holds course information for a student
#  and calculates their GPA
class GPA():
    def __init__(self):
        self.courses = []

    def addCourse(self, grade):
        self.courses.append(grade)

    def calculate_gpa(self):
        total_grade_points = 0
        total_credit_hours = 0

        for course in self.courses:
            grade_points = self.LettertoGPA(course.getGrade())
            total_grade_points += grade_points * course.getCreditHours()
            total_credit_hours += course.getCreditHours()

        if total_credit_hours == 0:
            return 0
        else:
            return total_grade_points / total_credit_hours
        
    def LettertoGPA(self, grade):
        total = 0.0
        if grade == "A+":
            total += 4.33
        elif grade == "A":
            total += 4.0
        elif grade == "A-":
            total += 3.66
        elif grade == "B+":
            total += 3.33
        elif grade == "B":
            total += 3.0
        elif grade == "B-":
            total += 2.66
        elif grade == "C+":
            total += 2.33
        elif grade == "C":
            total += 2.0
        elif grade == "C-":
            total += 1.66
        elif grade == "D+":
            total += 1.33
        elif grade == "D":
            total += 1.0
        elif grade == "D-":
            total += 0.66
        elif grade == "F":
            total += 0.0
        else:
            raise ValueError("Bad grade")
            
        return total

    def calculateGPA(self):
        total = 0.0
        for grade in self.courses:
            if grade == "A+":
                total += 4.33
            elif grade == "A":
                total += 4.0
            elif grade == "A-":
                total += 3.66
            elif grade == "B+":
                total += 3.33
            elif grade == "B":
                total += 3.0
            elif grade == "B-":
                total += 2.66
            elif grade == "C+":
                total += 2.33
            elif grade == "C":
                total += 2.0
            elif grade == "C-":
                total += 1.66
            elif grade == "D+":
                total += 1.33
            elif grade == "D":
                total += 1.0
            elif grade == "D-":
                total += 0.66
            elif grade == "F":
                total += 0.0
            else:
                raise ValueError("Bad grade")
            
        return total/len(self.courses)
    
    def GPAtoLetter(self,gpa):
        if gpa < 0.66:
            return "F"
        elif gpa <= 1.0 and gpa > 0.65:
            return "D-"
        elif gpa <= 1.33 and gpa > 0.99:
            return "D"
        elif gpa <= 1.66 and gpa > 1.32:
            return "D+"
        elif gpa <= 2.0 and gpa > 1.65:
            return "C-"
        elif gpa <= 2.33 and gpa > 1.99:
            return "C"
        elif gpa <= 2.66 and gpa > 2.32:
            return "C+"
        elif gpa <= 3.00 and gpa > 2.65:
            return "B-"
        elif gpa <= 3.33 and gpa > 2.99:
            return "B"
        elif gpa <= 3.66 and gpa > 3.32:
            return "B+"
        elif gpa <= 4.00 and gpa > 3.65:
            return "A"
        else:
                raise ValueError("Bad grade")
        
def final_grade_calculator(currentGrade:float, desiredGrade:float, finalWeight:float)-> float:
        """
        Calculates what a student will have to get on the final to achive a certain grade in the class.
        Input: Current grade in class, desired grade in class, and the weight of the final
        Output: Grade which the student needs to achive the grade they desire (float)
        """

        # This checks if the final is worth 0% of the grade and avoids a divide by zero error
        if finalWeight == 0:
            return 0
        
        # if the input is over 1 for any of the inputs, it converts it to the correct percent
        if currentGrade > 1.0:
            currentGrade *= 0.01
        if desiredGrade > 1.0:
            desiredGrade *= 0.01
        if finalWeight > 1.0:
            finalWeight *= 0.01

        grade = round((desiredGrade - currentGrade*((1 - finalWeight)))/finalWeight,2)

        return (f'You will need to get a {grade * 100}% on your final to achieve a {desiredGrade * 100}% in the class.')


    

