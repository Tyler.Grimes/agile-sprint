courses = []

file_path = "path/to/your/file.txt"  # Replace with the actual path to your file

try:
    with open(file_path, "r") as file:
        for line in file:
            courses.append(line.strip())  # Remove leading/trailing whitespace from each line
except FileNotFoundError:
    print(f"File not found: {file_path}")
except IOError as e:
    print(f"Error reading the file: {e}")
